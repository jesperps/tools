#!/usr/bin/env ruby
 
require 'rest-client'
require 'json'
require 'slop'
require 'pp'
 
#BEGIN define inparameters
opts = Slop.parse do |o|
    o.string \
        '-d'\
        , '--date'\
        , 'a date for indicating files for removal, files modified before this date will be removed'\
        , default: (Date.parse(Time.now.to_s) << 1).to_s
    o.bool \
        '-t'\
        , '--testrun'\
        , 'set true for not executing the delete statements, just print out the affected files'\
        , default: false
    o.string \
        '-q'\
        , '--queryfile'\
        , 'a file containing a aql query'
    o.string \
        '-h'\
        , '--hostname'\
        , 'hostname or ipadress to the artifactory server'\
        , default: '10.0.0.161'
    o.integer \
        '-p'\
        , '--port'\
        , 'port to the artifactory server'\
        , default: 8081
    o.string \
        '-c'\
        , '--credentialsfile'\
        , 'a file containing login creadentials for the artifactory'
    o.integer \
        '-k'\
        , '--snapstokeep'\
        , 'number of snapshot-directories for each artifact to keep regardless of date'\
        , default: 1
end #END define inparameters
 
#BEGIN Get queryfile content or use default query
unless opts[:queryfile]
    query = ''\
        'items.find('\
            '{"repo":'\
                '{"$match":"*-snapshot"}'\
                ',"type":"file"'\
                ',"$or":['\
                    '{"updated":{"$lt":"'+opts[:date]+'"}}'\
                    ',{"modified":{"$lt":"'+opts[:date]+'"}}'\
                ']'\
            '}'\
        ').include("repo", "path", "name")'
else
    file = File.open(opts[:queryfile])
    query = file.read
    file.close
end #END Get queryfile content or use default query
 
#BEGIN Get credentials file content or use default credentials
unless opts[:creadentials]
    credentials = 'admin:password'
else
    file = File.open(opts[:credentials])
    credentials = file.read
    file.close
end #END Get credentials file content or use default credentials
 
#BEGIN Get results from server
baseuri = 'http://'+credentials+'@'+opts[:hostname]+':'+opts[:port].to_s+'/artifactory'
searchuri = baseuri+'/api/search/aql'
resp = RestClient.post searchuri, query
results = JSON.parse(resp)
#END Get results from server
 
#BEGIN Collect direcotries with redundant files
# Every snapshot but the last in each snashot directory is redundant
paths = Array.new
results["results"].each do |result|
    paths.push(result["repo"]+'/'+result["path"])
end
snapdirs = paths.grep(/SNAPSHOT$/).uniq
snapdirbases = Array.new
snapdirs.each do |path|
    snapdir = path.split('/').last
    snapdirbases.push(path.sub(/#{snapdir}$/,''))
end
snapdirsbybase = Hash.new []
snapdirbases.sort.uniq.each do |base|
    snapdirs.each do |snapdir|
        if snapdir =~ /^#{base}/
            snapdirsbybase[base] += [snapdir]
        end
    end
    opts[:snapstokeep].times do #remove the n last dir, which is not redundant
        snapdirsbybase[base].pop
    end
end
snapdirstoempty = Array.new
snapdirsbybase.each do |base|
    snapdirstoempty.push(base[1])
end
snapdirstoempty = snapdirstoempty.reject{|e| e.empty?} #remove empty values
#END Collect directories with redundant files
 
#BEGIN Create delete statements matching the redundant direcories and send delete command
snapdirstoempty.flatten.each do |base|
    puts 'Delteing snapshotfiles in directory: ' + base
    results["results"].each do |result|
        if base.eql?(result["repo"]+'/'+result["path"])
            deleteuri = baseuri+"/"+result["repo"]+"/"+result["path"]+"/"+result["name"]
            puts '    Uri: ' + deleteuri.sub(/#{credentials}@/, '')
            unless opts[:testrun]
                RestClient.delete deleteuri
            end
        end
    end
end #END Create delete statements matching the redundant direcories and send delete command