#!/bin/bash
 
HOSTIP=$1
STATUS=`curl -s $HOSTIP:8080/health|sed 's/[\{\}\"]//g'|sed 's/[:,]/\ /g'|awk '{print $2}'`
 
if [[ $STATUS == "UP" ]]; then
    echo "OK - Service is $STATUS"
    exit 0
elif [[ $STATUS = ""  ]]; then
    echo "UNKNOWN - Status could not be retrieved"
    exit 3
else
    echo "CRITICAL - Service is $STATUS"
    exit 2
fi