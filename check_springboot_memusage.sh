#!/bin/bash
 
declare -A METRICS=();
 
HOSTIP=$1
 
for METRIC in `curl -s $HOSTIP:8080/metrics|sed 's/[\{\}\"\]//g'|sed 's/\,/ /g'`; do
    IFS=':' read -ra KEYVALUE <<<"$METRIC";
    METRICS+=([${KEYVALUE[0]}]=${KEYVALUE[1]});
done;
 
PERCENTAGEFREE=`bc -l<<<"scale=2; ${METRICS[mem.free]}/${METRICS[mem]}"|sed 's/^\.//g'|sed 's/^0//g'`
 
if (($PERCENTAGEFREE>=20)); then
    echo "OK - $PERCENTAGEFREE percent of memory free."
    exit 0
elif (($PERCENTAGEFREE<20 && $PERCENTAGEFREE>=10)); then
    echo "WARNING - $PERCENTAGEFREE percent of memory free"
    exit 1
elif (($PERCENTAGEFREE<10)); then
    echo "CRITICAL - $PERCENTAGEFREE percent of memory free"
    exit 2
else
    echo "UNKNOWN - memory status could not be retrieved"
    exit 3
fi