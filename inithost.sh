#!/bin/bash

while getopts 'i:p:l:h:d:n:' opt; do
  case $opt in
    i) IP="$OPTARG"
    ;;
    p) PATHEN="$OPTARG"
    ;;
    l) LNK="$OPTARG"
    ;;
    h) HOST="$OPTARG"
    ;;
    d) DOMN="$OPTARG"
    ;;
    n) DNSIP="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done

#echo $IP
#echo $PATHEN
#echo $LNK
#echo $HOST
#echo $DOMN
#echo $DNSIP

if [ "$PATHEN" = "" ]; then 
    PATHEN="/etc/sysconfig/network-scripts" 
fi

if [ "$LNK" = "" ]; then 
    LNK="enp0s3" 
fi

if [ "$IP" != "" ]; then
    sed -E -i "s/^IPADDR=([0-9]{1,3}\.){3}[0-9]{1,3}/IPADDR=$IP/" $PATHEN/ifcfg-$LNK
    
    GW=`echo $IP|sed s'/\./ /g'|awk '{print $1"."$2"."$3".1"}'`
    sed -E -i "s/^GATEWAY=([0-9]{1,3}\.){3}[0-9]{1,3}/GATEWAY=$GW/" $PATHEN/ifcfg-$LNK
    sed -E -i "s/^BOOTPROTO=dhcp/BOOTPROTO=none/" $PATHEN/ifcfg-$LNK
else
    sed -E -i "s/^BOOTPROTO=[a-z].*/BOOTPROTO=dhcp/" $PATHEN/ifcfg-$LNK
fi

if [ "$HOST" != "" ] && [ "$DOMN" != "" ]; then 
    hostnamectl set-hostname $HOST.$DOMN 
fi

if [ "$DOMN" != "" ]; then 
    sed -E -i "s/^DOMAIN=[a-z].*/DOMAIN=$DOMN/" $PATHEN/ifcfg-$LNK
fi 

if [ "$DNSIP" != "" ] && $(grep -q ^DNS $PATHEN/ifcfg-$LNK); then 
    sed -E -i "s/^DNS1=([0-9]{1,3}\.){3}[0-9]{1,3}/DNS1=$DNSIP/" $PATHEN/ifcfg-$LNK
elif [ "$DNSIP" != "" ] && $(grep -vq ^DNS $PATHEN/ifcfg-$LNK); then
    echo "DNS1=$DNSIP" >> $PATHEN/ifcfg-$LNK
else
    sed -E -i "/^DNS[1-9]/d" $PATHEN/ifcfg-$LNK
fi

#byt mac
MAC=`ip addr|grep link/ether|awk '{print $2}'`
sed -E -i "s/[0-9A-F:]{17}/$MAC/" $PATHEN/ifcfg-$LNK
