#!/usr/bin/env ruby
 
require 'rest-client'
require 'json'
require 'slop'
 
opts = Slop.parse do |o|
    o.string \
        '-d' \
        , '--date' \
        , 'a date for indicating files for removal, files modified before this date will be removed' \
        , default: (Date.parse(Time.now.to_s) << 1).to_s #defaults now - 1 month
    o.bool \
        '-t' \
        , '--testrun' \
        , 'set true for not executing the delete statements, just print out the affected files' \
        , default: false
end
 
query = ''\
    'items.find('\
        '{"repo":'\
            '{"$match":"*-snapshot"}'\
            ',"type":"file"'\
            ',"$or":['\
                '{"updated":{"$lt":"'+opts[:date]+'"}}'\
                ',{"modified":{"$lt":"'+opts[:date]+'"}}'\
            ']'\
        '}'\
    ').include("repo", "path", "name")'
 
baseuri = 'http://admin:password@10.0.0.161:8081/artifactory'
searchuri = baseuri + '/api/search/aql'
resp = RestClient.post searchuri, query
results = JSON.parse(resp)
 
results["results"].each do |result|
    deleteuri = baseuri + "/" + result["repo"] + "/" + result["path"] +"/" + result["name"]
    puts 'Remove: ' + deleteuri
    unless opts[:testrun]
        RestClient.delete deleteuri
    end
end