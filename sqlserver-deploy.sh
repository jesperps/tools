#!/bin/bash

while getopts 's:i:u:p:l:v:' opt; do
  case $opt in
    s) SERVER="$OPTARG"
    ;;
    i) INSTANCE="$OPTARG"
    ;;
    u) USR="$OPTARG"
    ;;
    p) PASSWORD="$OPTARG"
    ;;
    l) FILELOC="$OPTARG"
    ;;
    v) VERBOSITY="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done

if [ "$USR" = "" ]; then
    USR="`echo $USER|sed 's/FGIROT\\\//g'`"
fi

if [ "$PASSWORD" = "" ]; then
	printf "password:"
	read -s PASSWORD
	echo
fi

if [ "$FILELOC" = "" ]; then
    FILELOC="."
fi

if [ "$VERBOSITY" = "" ]; then
    VERBOSITY="false"
fi

URL="jdbc:jtds:sqlserver://$SERVER/master;instance=$INSTANCE;domain=FGIROT;user=$USR;password=$PASSWORD;ssl=request;"

for FILE in `ls -1 $FILELOC|grep .sql$|sort|sed 's/\ /#/g'`; do
	
	SCRIPT=$FILELOC/$FILE
	
	if file $FILE|grep 'UTF-8 Unicode (with BOM)' &> /dev/null; then
		SCRIPT="/tmp/NOBOM.sql"
		tail --bytes=+4 $FILE > $SCRIPT
	fi
    
	java -Duser.language=sv -Duser.country=SE -jar sqlworkbench.jar -script="`echo $SCRIPT|sed 's/#/\\ /g'`" -driver=net.sourceforge.jtds.jdbc.Driver -driverJar=jtds-1.3.1.jar -url=$URL -feedback="$VERBOSITY" -autocommit=true;
	
	if [ $SCRIPT = "/tmp/NOBOM.sql" ]; then
		rm $SCRIPT
	fi
done